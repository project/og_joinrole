
if (Drupal.jsEnabled) {
  $(document).ready(function(){
    //----- replaces any subscribe links
    theparent = $("a[@href*=og/subscribe]").parent();
    theparent.empty();
    theparent.append(og_joinrole_replacementtext);
    
    //----- replaces any "you must register/login in order..." text
    theli = $("div[@id^=block-og] li").contains("in order to post into this group");
    theli.empty();
    theli.append(og_joinrole_replacementtext);
  });
}
