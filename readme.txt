DESCRIPTION
--------------------------
Enable site admins to limit which roles can join organic groups.

INSTALLATION
---------------
- Organic Groups module is required. 
- Enable og_joinrole module
- Define which roles may join Organic Groups via admin/user/access
- og_joinrole settings (admin/og/og_joinrole) allow you to edit the replacement text that is displayed to the user when they don't have "subscribe" permission.

NOTES
----------------
- og_joinrole uses jquery to substitute "og/subscribe" links with anywhere on the page
- og_joingrole.module and og_joinrole.js has some English language text that can be changed to further modify replacement text
- og_joinrole.module must have a higher module weight than OG in order to override the "og/subscribe" menu item. When installed, this is automatically handled. If the module weight of OG is manually changed, then the module weight of og_joinrole must be manually changed to be higher than OG as well.


TODO/BUGS/FEATURE REQUESTS
----------------
- see http://drupal.org/project/issues/og_joinrole

CREDITS
----------------------------
Authored and maintained by michael anello <manello AT gmail DOT com>
Sponsored by Florida Green Building Coalition - http://www.floridagreenbuilding.org/
